package models;


import javax.persistence.*;

@Entity
public class ComputerSpecification {

    @Id
    @GeneratedValue
    public int specId;
    /*
        public enum os{};

        public enum cpu{};

        public enum gpu{};
    */
    public int ramSize;

    @ManyToOne
    @JoinColumn(name = "user")
    public User user;

}
