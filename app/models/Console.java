package models;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Console {

    @Id
    public String consoleName;

    public String manufacturer;

    @ManyToMany(mappedBy = "consoles")
    public Collection<User> owners;

}
