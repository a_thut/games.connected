package models;

import org.hibernate.type.TextType;
import play.db.jpa.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Flo on 19.05.2015.
 */

public class Text {

    public Date postDate;

    public String texttitle;

    @Lob //large text database type to store post content JPA
    public String textcontent;

    public TextType type;

    @ManyToOne
    @JoinColumn(name = "author")
    public User author;

    @ManyToOne
    @JoinColumn(name = "game")
    public Game game;

    @OneToOne
    public Text text;


    @OneToMany(mappedBy="text", cascade=CascadeType.ALL)
    public List<Reply> replys;

    public Text(User author, String texttitle, String textcontent,TextType type) {
        this.replys = new ArrayList<Reply>();
        this.author = author;
        this.texttitle = texttitle;
        this.textcontent = textcontent;
        this.type = type;
        this.postDate = new Date();
    }

    public void save() {

        JPA.em().persist(this);
    }
}
