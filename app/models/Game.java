package models;

import play.data.validation.Constraints;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import java.net.URL;
import java.util.Collection;

/**
 * Created by Andreas on 20.05.2015.
 * Edited by Nina on 22.05.2015
 */
public class Game {
    @Id
    public String name;

    @Constraints.Required
    public Genre genre;
    public String publisher;
    public String description;
    public String hashtag;
    public int rated;
    public URL affiliateLink;

    @OneToMany(mappedBy = "game")
    public Collection<Media> mediaCollection;

    @OneToMany(mappedBy = "game")
    public Collection<Text> textCollection;

    @OneToMany(mappedBy = "game")
    public Collection<GameVersion> versionCollection;
}
