package models;

import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class User {

    @Id
    public String userName;

    @Constraints.Required
    public String password;

    public String firstName;

    public String lastName;

    public String picture;

    @ManyToMany
    public Collection<Console> consoles;

    @ManyToMany
    public Collection<GameVersion> gameVersions;

    @OneToMany(mappedBy = "author")
    public Collection<Text> text;

    @OneToMany(mappedBy = "user")
    public Collection<ComputerSpecification> computerSpecifications;
}
