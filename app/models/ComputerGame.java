package models;

import javax.persistence.*;

/**
 * Created by Andreas on 20.05.2015.
 */

@Entity
@DiscriminatorValue("Computer")
public class ComputerGame extends GameVersion {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SPEC_ID")
    public ComputerSpecification requiredSpec;
}
