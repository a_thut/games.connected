
package models;

import play.db.jpa.*;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Flo on 19.05.2015.
 */

//@Entity
public class Reply {

    public User author;
    public Date postDate;

    @Lob
    public String textcontent;

    @ManyToOne
    public Text text;

    public Reply(Text text, User author, String textcontent){
        this.text= text;
        this.textcontent= textcontent;
        this.author=author;
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY,0);
        this.postDate= today.getTime();
    }

    public void save() {

        JPA.em().persist(this);
    }


}
