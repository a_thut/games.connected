package models;

import play.data.validation.Constraints;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.net.URL;

import java.awt.PageAttributes.MediaType;

/**
 * Created by Nina on 22.05.2015.
 */
public class Media {
    @Id
    public URL url;

    @Constraints.Required
    public MediaType type;

    @ManyToOne
    @JoinColumn(name = "game")
    public Game game;
}
