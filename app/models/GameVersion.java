package models;

import org.hibernate.annotations.DiscriminatorOptions;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Andreas on 20.05.2015.
 */

@Entity
@Inheritance
@DiscriminatorColumn(name = "GAME_TYPE")
@DiscriminatorOptions(force = true)
public abstract class GameVersion {

    @Id
    @GeneratedValue
    public Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "game")
    public Game game;

    public Date releaseDate;

    @ManyToMany(mappedBy = "gameVersions")
    public Collection<User> players;
}
