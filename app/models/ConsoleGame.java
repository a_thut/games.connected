package models;

import javax.persistence.*;

/**
 * Created by Andreas on 20.05.2015.
 */

@Entity
@DiscriminatorValue("Console")
public class ConsoleGame extends GameVersion {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONSOLE")
    public Console console;

}
